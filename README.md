# README #

### What is this repository for? ###

* Creating On Point Tech
* Storing files for our app

### How do I get set up? ###

* Ask RealEthanSmith for help setting up

### Contribution guidelines ###

* Only make sensible or nessecary changes

### What do I do if I need help? ###

* Talk to RealEthanSmith
* Look at the Help Code file in sources (it's also a download file titled "main.m")

### What software do I use? ###

* Xcode

### What programming languages are used? ###

* Swift